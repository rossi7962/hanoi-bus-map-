#include "lib/jrb.h"
#include "lib/dllist.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define INFINITIVE_VALUE 1000000

typedef struct{
  JRB edges;
  JRB vertices;
}Graph;



Graph createGraph(){
  Graph tree;
  tree.edges = make_jrb();
  tree.vertices = make_jrb();
  return tree;
}

JRB getVertex(Graph graph,char *route){
  JRB ptr;
  jrb_traverse(ptr,graph.vertices)
    {
      if( strncmp(route,jval_s(ptr->val),1000) == 0 )
	{
	  return ptr;
	}
    }
  /* JRB vertex = jrb_find_int(graph.vertices,id); */
  /* if(vertex != NULL) return jval_s(vertex->val); */
  /* else return NULL; */
  return NULL;
}

void addVertex(Graph graph,int id,char* name)
{
  JRB vertex = jrb_find_int(graph.vertices,id);
  if(vertex == NULL)
    {
      jrb_insert_int(graph.vertices,id,new_jval_s(name));
    }
  else
    {
      vertex->val = new_jval_s(name);
    }
}

int hasEdge(Graph graph, int v1, int v2){
  JRB tree;
  JRB node = jrb_find_int(graph.edges,v1);
  if (node != NULL)
    {
      tree = (JRB)jval_v(node->val);
      if( jrb_find_int(tree,v2) != NULL )
	return 1;       
    }
  return 0;
  
}

void addEdge(Graph graph,int v1,int v2){
  JRB tree;
  JRB n1 = jrb_find_int(graph.vertices,v1);
  JRB n2 = jrb_find_int(graph.vertices,v2);
  if( n1 == NULL )
    {
      //printf("\n\n Pls add vertex %d before add edge\n\n",v1);
      return;
    }
  if( n2 == NULL )
    {
      //printf("\n\n Pls add vertex %d before add edge\n\n",v2);
      return;
    }
  if( hasEdge(graph,v1,v2) )
    {
      //printf("\n\nAlready has edge: %d & %d\n\n",v1,v2);
      return;
    }

  JRB node = jrb_find_int(graph.edges,v1);
  if(node == NULL )
    {
      tree = make_jrb();
      jrb_insert_int(graph.edges,v1,new_jval_v(tree));      
    }
  else
    {
      tree = (JRB)jval_v(node->val);
    }
  jrb_insert_int(tree,v2,new_jval_i(0));
}


int InDegree (Graph graph, int v, int* output)
{
  JRB tree, node;
  int total = 0;   
  jrb_traverse(node, graph.edges)
    {
      tree = (JRB) jval_v(node->val);
      if (jrb_find_int(tree, v))
	{
          output[total] = jval_i(node->key);
          total++;
	}                
    }
  return total;   
}

int OutDegree(Graph graph,int v,int* output){
  JRB node = jrb_find_int(graph.edges,v);
  if(node == NULL)
    return 0;
  JRB tree = (JRB)jval_v(node->val);
  int total = 0;
  int count = 0;
  JRB cur;
  jrb_traverse(cur,tree)
    {      
      *(output + total) = jval_i(cur->key);
      total += 1;
    }
  return total;
}

void BFS(Graph graph,int start,int stop,void (*func)(int))
{
  Dllist queue = new_dllist();
  JRB visit = make_jrb();
  dll_append(queue,new_jval_i(start));
  while( !dll_empty(queue) )
    {
      Dllist node = dll_first(queue);
      int u = jval_i(node->val);
      dll_delete_node(node);
      if( jrb_find_int(visit,u) == NULL)
	{
	  func(u);
	  jrb_insert_int(visit,u,new_jval_i(0));
	}
      if( u == stop) break;
      int output[100];
      int n = OutDegree(graph,u,output);
      for(int i = 0; i < n; i += 1 )
	{
	  if( jrb_find_int(visit,output[i]) == NULL)
	    dll_append(queue,new_jval_i(output[i]));
	}
    }
}

void DFS(Graph graph,int start,int stop,void (*func)(int))
{
  Dllist stack = new_dllist();
  JRB visit = make_jrb();
  dll_append(stack,new_jval_i(start));
  while( !dll_empty(stack) )
    {
      Dllist node = dll_last(stack);
      int u = jval_i(node->val);
      dll_delete_node(node);
      if( jrb_find_int(visit,u) == NULL)
	{
	  func(u);
	  jrb_insert_int(visit,u,new_jval_i(0));
	}
      if( u == stop) break;
      int output[100];
      int n = OutDegree(graph,u,output);
      for(int i = 0; i < n; i += 1 )
	{
	  if( jrb_find_int(visit,output[i]) == NULL)
	    dll_append(stack,new_jval_i(output[i]));
	}
    }
}

int shortestPath(Graph graph, int start, int stop, int *path){
    Dllist queue = new_dllist();
    JRB visited = make_jrb();
    JRB prevTable = make_jrb();

    JRB startNode = jrb_find_int(graph.vertices, start);
    if(startNode == NULL) return 0;

    int prev = 0;
    int v = start;

    dll_append(queue, new_jval_i(start));
    jrb_insert_int(prevTable, start, new_jval_i(prev));

    while(!dll_empty(queue)){
        //dequeue
        Dllist node = dll_first(queue);
        v = jval_i(node->val);
        dll_delete_node(node);

        if(jrb_find_int(visited, v) == NULL){
            jrb_insert_int(visited, v, new_jval_i(1));
        }

        if(v == stop)
            break;

        prev = v;
        int output[100];
        int n = OutDegree(graph, v, output);
        int i;
        for(i = 0; i < n; i++){
            if(jrb_find_int(visited, output[i]) == NULL){
                dll_append(queue, new_jval_i(output[i]));
                jrb_insert_int(prevTable, output[i], new_jval_i(prev));
            }
        }
    }

    //check if the last node visited is stop
    int counter = 0;
    if(v == stop){
    	JRB ptr;
    	int ptrIndex = v;
        path[counter] = ptrIndex;
        counter++;
    	while(ptrIndex != start){
    		ptr = jrb_find_int(prevTable, ptrIndex);
    		ptrIndex = jval_i(ptr->val);
    		path[counter] = ptrIndex;
    		counter++;
    	}

        //reverse path
        int i = 0, j = counter - 1, temp;
        while(i < j){
            temp = path[i];
            path[i] = path[j];
            path[j] = temp;
            i++;
            j--;
        }
    }

    
    jrb_free_tree(visited);
    jrb_free_tree(prevTable);
    free_dllist(queue);

    return counter;
}	 

void printVertex(int v) { printf("%4d", v); }

struct dup{
  int node;
  JRB dupnode;
};

int is_dup(int search,struct dup arr[],int size)
{
  for(int j = 0; j < size; j += 1)
    {
      JRB node = jrb_find_int(arr[j].dupnode,search);
      if(node != NULL)
	{
	  return arr[j].node;
	}
    }
  return -1;
}


int main(){
  //Read the bus map data

  JRB bus = make_jrb();
  Graph map = createGraph();
  int dup_node[100000];
  //int convert[100000];
  struct dup dup_data[10000];
  for(int index = 0; index < 10000 ; index += 1)
    {
      dup_data[index].dupnode = make_jrb();
    }
  FILE *in = fopen("data.txt","r");
  char string[10000];
  int i = 0;
  int j = 0;
  while( fgets(string,10000,in) != NULL )
    {
      string[strlen(string) - 1] = '\0';
      char *tok;
      tok = strtok(string,":");
      if( tok == NULL ) continue;
      char *name = strdup(tok);
      JRB tree = make_jrb();     
      while( tok != NULL )
      	{
      	  tok = strtok(NULL,"-");
	  if(tok == NULL) continue;
	  char *route = strdup(tok);
	  //check if this station is already in the map
	  JRB get = getVertex(map,route);
	  if(get != NULL)
	    {
	      //note the duplicate index
	      dup_data[j].node = jval_i(get->key);
	      jrb_insert_int(dup_data[j].dupnode,i,new_jval_i(-1));
	      j += 1;	  
	      
	    }
	  else
	    addVertex(map,i,route);
	  //insert the station into the tree
	  jrb_insert_int(tree,i,new_jval_s(route));	 
	  i += 1;
      	}
      //insert the bus into the bus tree
      jrb_insert_str(bus,name,new_jval_v(tree));
    }
  JRB ptr;
  //traverse the bus tree to connect the vertices in the Graph
  jrb_traverse(ptr,bus)
    {
      //printf("bus %s: \n",jval_s(ptr->key));
      JRB station = (JRB)jval_v(ptr->val);
      JRB point;
      jrb_traverse(point,station)
      	{
	  if( jrb_next(point) != NULL)
	    {
	      
	      int v1 = jval_i(point->key);
	      int v2 = jval_i(jrb_next(point)->key);
	      
	      if( is_dup(v1,dup_data,10000) != -1)
		v1 = is_dup(v1,dup_data,10000);
	      if( is_dup(v2,dup_data,10000) != -1)
		v2 = is_dup(v2,dup_data,10000);
		
	      addEdge(map,v1,v2);
	      addEdge(map,v2,v1);
	      /* printf("Added %s & %s\n", */
	      /* 	     jval_s(point->val), */
	      /* 	     jval_s(jrb_next(point)->val)); */
	    }
      	}
    }

  int path[100000];

  printf("Enter Starting Station: ");
  char find1[1000];// = strdup("Bx Gia Lam");
  fgets(find1,1000,stdin);
  find1[strlen(find1) - 1] = '\0';
  JRB pos1 = getVertex(map,find1);
  if(pos1 == NULL)
    {
      printf("%s not found!\n",find1);
      return 0;
    }

  printf("Enter Destination: ");
  char find2[1000];// = strdup("Cau Chuong Duong");
  fgets(find2,1000,stdin);
  find2[strlen(find2) - 1] = '\0';
  JRB pos2 = getVertex(map,find2);
  if(pos2 == NULL)
    {
      printf("%s not found!\n",find2);
      return 0;
    }
  
  int start = jval_i(pos1->key);
  int end = jval_i(pos2->key);
  int length = shortestPath(map,start,end,path);

  
  printf("\nShortest Path: ");
  for(int count = 0; count < length; count += 1)
    {
      printf("%s ",jval_s(jrb_find_int(map.vertices,
				       path[count])->val));
      if(count != length - 1)
	printf("-> ");
    }
  printf("\n");
}

//Lecturer 's email: ducnh@soict.hust.edu.vn
